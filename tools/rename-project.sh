#!/bin/bash

if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "-?" ]; then
	cat <<EOF
./add-feature-template.sh <CurrentName> [NewName]

CurrentName    Optional parameter, specifies the current name of the project
               Default value: ExamTemplate
Example: ./rename-project.sh CarShop
Example: ./rename-project.sh CarShop BookShop

For more information: https://gitlab.com/Syndamia/it-kariera-exam-template#rename-project
EOF
	exit
fi

if [ -z "$1" ]; then
	echo "No name supplied!"
	exit
fi

# Defaults
original_name="ExamTemplate"
new_name="$1"

if [ ! -z "$2" ]; then
	original_name="$1"
	new_name="$2"
fi

rename () {
	for file in $@; do
		mv $file ${file/$original_name/$new_name}
	done
}

mv ../$original_name ../$new_name

# Rename all directories and then files
rename $(find ../$new_name -type d -name "*$original_name*")
rename $(find ../$new_name -type f -name "*$original_name*")

# Rename all occurences inside files
find ../$new_name -type f -not -path "../tools/*" -not -path "../.git/*" -print0 | xargs -0 sed -i "s/$original_name/$new_name/g"

# The database context is named TemplateContext by default
# A third paramter should never be given, the second check is to prevent stack overflow
# if someone wants to name their project just "Template"
if [ "$original_name" != "Template" ] && [ "$3" != "Recursive" ]; then
	./rename-project.sh "Template" $new_name "Recursive"
fi
