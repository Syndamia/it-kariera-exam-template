#!/bin/bash

if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "-?" ]; then
	cat <<EOF
./rename-namespaces.sh <CurrentName|-r> [FeatureName]

CurrentName    An optional paramter, specifies the current start name of all namespaces.
               Default value: ExamTemplate
-r             An optional paramter, removes the specified start name of all namespaces.

Example: ./rename-namespaces.sh MyShop CarShop
Example: ./rename-namespaces.sh -r CarShop

For more information: https://gitlab.com/Syndamia/it-kariera-exam-template#rename-namespaces
EOF
	exit
fi

if [ -z "$1" ]; then
	echo "No name supplied!"
	exit
fi

# Defaults
original_name="ExamTemplate."
new_name="$1."

if [ "$1" = "-r" ]; then
	original_name="$2."
	new_name=""
elif [ ! -z "$2" ]; then
	original_name="$1."
	new_name="$2."
fi

keywords=("namespace" "using" "model")
for keyword in "${keywords[@]}"; do
	find ../ -type f -not -path "*obj*" -not -path "*bin*" -exec sed -i "s/$keyword $original_name/$keyword $new_name/g" {} \;
done
