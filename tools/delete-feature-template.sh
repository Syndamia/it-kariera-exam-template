#!/bin/bash

if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "-?" ]; then
	cat <<EOF
./delete-feature-template.sh [ProjectName] [FeatureName]

Example: ./delete-feature-template.sh CarShop Car

For more information: https://gitlab.com/Syndamia/it-kariera-exam-template#delete-feature-template
EOF
	exit
fi

if [ -z "$1" ] || [ -z "$2" ]; then
	echo "No name supplied!"
	exit
fi

project_name="$1"
feature_name="$2"

# {{{ =============================>
# Remove from Data layer

dir="../$project_name/Data"

rm -f $dir/${project_name}.Data.Models/$feature_name.cs
sed -i "/$feature_name/d" $dir/${project_name}.Data/${project_name}Context.cs

# }}} =============================<

# {{{ =============================>
# Remove from Service layer

dir="../$project_name/Services"

rm -f $dir/${project_name}.Services/Configurations/Service${feature_name}Mappings.cs
rm -rf $dir/${project_name}.Services.Models/$feature_name
rm -f $dir/${project_name}.Services/${feature_name}Service.cs

# }}} =============================<

# {{{ =============================>
# Remove from Web layer

dir="../$project_name/Web"

rm -f $dir/${project_name}.Web/Configurations/Controller${feature_name}Mappings.cs
rm -f $dir/${project_name}.Web/Controllers/${feature_name}Controller.cs
rm -rf $dir/${project_name}.Web.Models/$feature_name
rm -rf $dir/${project_name}.Web/Views/$feature_name
sed -i "/$feature_name/d" $dir/${project_name}.Web/Views/_ViewImports.cshtml
sed -i "/$feature_name/d" $dir/${project_name}.Web/Startup.cs

# }}} =============================<
