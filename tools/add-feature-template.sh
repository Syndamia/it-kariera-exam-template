#!/bin/bash

if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "-?" ]; then
	cat <<EOF
./add-feature-template.sh [ProjectName] [FeatureName] "[type Name]" "[type Name]" ...

Example: ./add-feature-template.sh CarShop Car "string Brand" "string Model" "int Year"

For more information: https://gitlab.com/Syndamia/it-kariera-exam-template#add-feature-template
EOF
	exit
fi

if [ -z "$1" ] || [ -z "$2" ]; then
	echo "No name supplied!"
	exit
fi

project_name="$1"
feature_name="$2"
features=("${@:3}")

if [ -z features ]; then
	features=("string Name")
fi

# {{{ =============================>
# Add to Data layer

dir="../$project_name/Data"

# {{{ --------------------------->
# Create the DB model class

cat > $dir/${project_name}.Data.Models/$feature_name.cs<< EOF
using System;

namespace $project_name.Data.Models
{
	public class $feature_name : BaseModel
	{
$(
for feature in "${features[@]}"; do
echo -e "\t\tpublic $feature { get; set; }"
done
)
	}
}
EOF

# }}} ---------------------------<

# {{{ --------------------------->
# Add the DbSet to DbContext 


dbset="public DbSet<$feature_name> ${feature_name}s { get; set; }"
dbcontext_path="$dir/${project_name}.Data/${project_name}Context.cs"

if ! grep -q "$dbset" "$dbcontext_path"; then
	if grep -Fq "DbSet" $dbcontext_path; then
		sed -z -i "s/.*DbSet[^\n]*\n/&\t\t$dbset\n/" $dbcontext_path
	else
		sed -i "14 i \\\t\t$dbset\n" $dbcontext_path
	fi
fi

# }}} ---------------------------<

# }}} =============================<

# {{{ =============================>
# Add to Service layer

dir="../$project_name/Services"

# {{{ --------------------------->
# Create Service and Interface

cat > $dir/${project_name}.Services/Interfaces/I${feature_name}Service.cs<< EOF
using $project_name.Data.Models;
using $project_name.Services.Models.$feature_name;

namespace $project_name.Services.Interfaces
{
	public interface I${feature_name}Service : IBaseService<${feature_name}, ${feature_name}ServiceModel>
	{ }
}
EOF

cat > $dir/${project_name}.Services/Services/${feature_name}Service.cs<< EOF
using $project_name.Services.Interfaces;
using System;
using AutoMapper;
using $project_name.Data;
using $project_name.Data.Models;
using $project_name.Services.Models.$feature_name;

namespace $project_name.Services.Services
{
	public class ${feature_name}Service : BaseService<${feature_name}, ${feature_name}ServiceModel>, I${feature_name}Service
	{
		public ${feature_name}Service(IMapper autoMapper, ${project_name}Context context)
			:base(autoMapper, context)
		{ }
	}
}
EOF

# }}} ---------------------------<

# {{{ --------------------------->
# Create Service Model

subdir="$dir/${project_name}.Services.Models/$feature_name"
mkdir -p $subdir

cat > $subdir/$model${feature_name}ServiceModel.cs<< EOF
using System;
using $project_name.Services.Models;

namespace $project_name.Services.Models.$feature_name
{
	public class $model${feature_name}ServiceModel : BaseServiceModel
	{
$(
for feature in "${features[@]}"; do
echo -e "\t\tpublic $feature { get; set; }"
done
)
	}
}
EOF

# }}} ---------------------------<

# {{{ --------------------------->
# Create Service Mappings

subdir="$dir/${project_name}.Services/Configurations"

mappings=("${feature_name}ServiceModel, $feature_name" "${feature_name}, ${feature_name}ServiceModel")
cat > $subdir/Service${feature_name}Mappings.cs<< EOF
using AutoMapper;
using $project_name.Data.Models;
using $project_name.Services.Models.$feature_name;

namespace $project_name.Services.Configurations
{
	public class Service${feature_name}Mappings : Profile
	{
		public Service${feature_name}Mappings()
		{
$(
for map in "${mappings[@]}"; do
echo -e "\t\t\tCreateMap<$map>();"
done
)
		}
	}
}
EOF

# }}} ---------------------------<

# }}} =============================<

# {{{ =============================>
# Add to Web layer

dir="../$project_name/Web"

# {{{ --------------------------->
# Add the dependency injection of the Service class

startup_path="$dir/${project_name}.Web/Startup.cs"
service_name="${feature_name}Service"
if ! grep -q "$service_name" "$startup_path"; then
	sed -z -i "s/.*AddTransient[^\n]*\n/&\t\t\tservices.AddTransient<I$service_name, $service_name>();\n/" $startup_path
fi

# }}} ---------------------------<

# {{{ --------------------------->
# Create View Models

subdir="$dir/${project_name}.Web.Models/$feature_name"
mkdir -p $subdir

models=("" "Create" "Edit")
for model in "${models[@]}"
do
cat > $subdir/$model${feature_name}WebModel.cs<< EOF
using System;$(
if [ "$model" != "Create" ]; then
echo -e "\nusing $project_name.Web.Models;"
fi
)

namespace $project_name.Web.Models.$feature_name
{
	public class $model${feature_name}WebModel$(
if [ "$model" != "Create" ]; then
echo -e ' : BaseWebModel'
fi
)
	{
$(
for feature in "${features[@]}"; do
echo -e "\t\tpublic $feature { get; set; }"
done
)
	}
}
EOF
done

# }}} ---------------------------<

# {{{ --------------------------->
# Update View Imports

viewimports_path="$dir/${project_name}.Web/Views/_ViewImports.cshtml"
import_name="$project_name.Web.Models.$feature_name"
if ! grep -q "$import_name" "$viewimports_path"; then
	sed -z -i "s/.*@using[^\n]*\n/&@using $import_name\n/" $viewimports_path
fi

# }}} ---------------------------<

# {{{ --------------------------->
# Create Views

subdir="$dir/${project_name}.Web/Views/$feature_name"
mkdir -p $subdir

cat > $subdir/Index.cshtml<< EOF
@model IEnumerable<${feature_name}WebModel>
@{
	ViewData["Title"] = "${feature_name}s";
}

<div>
	@foreach (var item in Model)
	{
		<section>
$(
for feature in "${features[@]}"; do
echo -e "\t\t\t<div>\n\t\t\t\t@item.$(echo $feature | cut -d ' ' -f 2-)\n\t\t\t</div>"
done
)
		</section>
	}
</div>
EOF

cat > $subdir/Details.cshtml<< EOF
@model ${feature_name}WebModel
@{
	ViewData["Title"] = "$feature_name";
}

<section>
$(
for feature in "${features[@]}"; do
echo -e "\t<div>\n\t\t@Model.$(echo $feature | cut -d ' ' -f 2-)\n\t</div>"
done
)
</section>
EOF

cat > $subdir/Create.cshtml<< EOF
@model Create${feature_name}WebModel
@{
	ViewData["Title"] = "Creating a new $feature_name";
}

<form asp-controller="$feature_name" asp-action="Create" method="post">
$(
for feature in "${features[@]}"; do
feature_name=$(echo $feature | cut -d ' ' -f 2-)
echo -e "\t<input type=\"text\" asp-for=\"$feature_name\" placeholder=\"$feature_name\">\n\t<span asp-validation-for=\"$feature_name\" class=\"form-error\"></span>\n"
done
)

	<input type="submit" value="Create">
</form>

@if (Model != null)
{
	<p class="form-error">
		Couldn't create ${feature_name}!
	</p>
}
EOF

cat > $subdir/Edit.cshtml<< EOF
@model Edit${feature_name}WebModel
@{
	ViewData["Title"] = "Editing $feature_name";
}

<form asp-controller="$feature_name" asp-action="Edit" method="post">
	<!-- Gets the id from the URL and sets it to the model -->
	<input type="hidden" asp-for="Id" value="@Context.Request.Query["id"]">
$(
for feature in "${features[@]}"; do
feature_name=$(echo $feature | cut -d ' ' -f 2-)
echo -e "\t<label asp-for=\"$feature_name\">$feature_name</label>\n\t<input type=\"text\" asp-for=\"$feature_name\" placeholder=\"$feature_name\">\n\t<span asp-validation-for=\"$feature_name\" class=\"form-error\"></span>\n"
done
)

	<input type="submit" value="Edit">
</form>

@if (Model != null)
{
	<p class="form-error">
		Couldn't edit ${feature_name}!
	</p>
}
EOF

cat > $subdir/Delete.cshtml<< EOF
@{
	ViewData["Title"] = "Deleting $feature_name";
}

<p>
	Are you sure you want to delete this $feature_name?
</p>
<form asp-controller="$feature_name" asp-action="Delete" method="post">
	<!-- Gets the id from the URL and sets it to the model -->
	<input type="hidden" name="id" value="@Context.Request.Query["id"]">
	
	<input type="submit" value="Delete">
</form>
EOF

# }}} ---------------------------<

# {{{ --------------------------->
# Create Controller

subdir="$dir/${project_name}.Web/Controllers"

cat > $subdir/${feature_name}Controller.cs<< EOF
using System;
using System.Threading.Tasks;
using AutoMapper;
using $project_name.Services.Interfaces;
using $project_name.Services.Models.$feature_name;
using $project_name.Web.Models.$feature_name;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace $project_name.Web.Controllers
{
	public class ${feature_name}Controller : Controller
	{
		private readonly IMapper _autoMapper;
		private readonly I${feature_name}Service _service;

		public ${feature_name}Controller(IMapper autoMapper, I${feature_name}Service service)
		{
			this._autoMapper = autoMapper;
			this._service = service;
		}

		[HttpGet]
		public async Task<IActionResult> Index()
		{
			List<${feature_name}ServiceModel> serviceModels = await this._service.GetAllAsync();

			List<${feature_name}WebModel> webModel = serviceModels
				.Select(x => this._autoMapper.Map<${feature_name}WebModel>(x))
				.ToList();
			return View(webModel);
		}

		[HttpGet]
		public async Task<IActionResult> Details(string id)
		{
			${feature_name}ServiceModel serviceModel = await this._service.GetByIdAsync(Guid.Parse(id));

			${feature_name}WebModel webModel = this._autoMapper.Map<${feature_name}WebModel>(serviceModel);
			return View(webModel);
		}


		[HttpGet]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Create(Create${feature_name}WebModel webModel)
		{
			if (!ModelState.IsValid)
				return View(webModel);

			${feature_name}ServiceModel serviceModel = this._autoMapper.Map<${feature_name}ServiceModel>(webModel);

			bool result = await this._service.CreateAsync(serviceModel);

			if (result)
				return RedirectToAction("Index");
			else
				return RedirectToAction("Create");
		}

		[HttpGet]
		public async Task<IActionResult> Edit(string id)
		{
			${feature_name}ServiceModel serviceModel = await this._service.GetByIdAsync(Guid.Parse(id));

			Edit${feature_name}WebModel edit${feature_name}WebModel = this._autoMapper.Map<Edit${feature_name}WebModel>(serviceModel);
			return View(edit${feature_name}WebModel);
		}

		[HttpPost]
		public async Task<IActionResult> Edit(Edit${feature_name}WebModel webModel)
		{
			if (!ModelState.IsValid)
				return View(webModel);

			${feature_name}ServiceModel serviceModel = this._autoMapper.Map<${feature_name}ServiceModel>(webModel);

			bool result = await this._service.EditAsync(serviceModel);

			if (result)
				return RedirectToAction("Details", new { id = webModel.Id.ToString() });
			else
				return RedirectToAction("Edit", new { id = webModel.Id.ToString() });
		}

		[HttpGet]
		public IActionResult Delete()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Delete(string id)
		{
			bool result = await this._service.DeleteAsync(Guid.Parse(id));

			if (result)
				return RedirectToAction("Index");
			else
				return RedirectToAction("Delete", new { id = id });
		}
	}
}
EOF

# }}} ---------------------------<

# {{{ --------------------------->
# Create Controller Mappings

subdir="${dir}/${project_name}.Web/Configurations"

mappings=("${feature_name}ServiceModel, ${feature_name}WebModel" "Create${feature_name}WebModel, ${feature_name}ServiceModel" "${feature_name}ServiceModel, Edit${feature_name}WebModel" "Edit${feature_name}WebModel, ${feature_name}ServiceModel")
cat > $subdir/Controller${feature_name}Mappings.cs<< EOF
using AutoMapper;
using $project_name.Services.Models.$feature_name;
using $project_name.Web.Models.$feature_name;

namespace $project_name.Web.Configurations
{
	public class Controller${feature_name}Mappings : Profile
	{
		public Controller${feature_name}Mappings()
		{
$(
for map in "${mappings[@]}"; do
echo -e "\t\t\tCreateMap<$map>();"
done
)
		}
	}
}
EOF

# }}} ---------------------------<

# }}} =============================<
