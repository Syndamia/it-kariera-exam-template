# IT-kariera Exam Template

A template that could be useful for the final IT-kariera exam in 2021. Special thanks to [Danail Dimitrov](https://github.com/Danail-Dimitrov) and [Victor Stamoff](https://github.com/transtrike).

Oh, also, it's garbage, don't use it. A **lot** of stuff are broken and semi-working (even in the README T\_T). Also, there are probably a lot of badly written things so yeah....

## Getting started

Outside of having installed and configured ASP.NET and a database, all you need to do is:
- Create an `appsettings.Develoment.json` file in `ExamTemplate/Web` that is a direct copy of `appsettings.json`, but you put in your connection string
- Optional: Change the database the project uses (refer to [Database](#database))

The template is setup to automatically apply migrations and it comes by default with some.

## Structure

This project uses some main layers. Those layers are the projects inside the solution. Their purpose is:
- Common: Mainly constants and things that could be needed everywhere in the app
- Data: Contains the project context, the context (database) models and migrations
- Service: Contains the business logic, does stuff with the database
- Web: the whole front-end - Views, View models and Controllers

In some layers you have `Configurations` (Web and Service). Put your Automapper configurations there.

The `css` files can be found in `ExamTemplate/Web/wwwroot/css/`. The main stuff are put in `styles.css` and that's where you probably should put your own stuff.

## Tools

The template comes with some very useful tools, found inside `ExamTemplate/tools/`. They are all Bash scripts, so to run them:
- Windows users could just use [git bash](https://gitforwindows.org/). Open a window inside the `ExamTemplate/tools/` folder, type in `./tool-name.sh` and then your parameters (so a command should be something like `./rename-project.sh CarShop`).
- Linux and MacOS users could just use their terminal. **But**, beware: the scripts aren't POSIX compatible, so you might have issues trying to run them.

**No matter the OS, test out the scripts before the exam!** Also, **run these scripts ONLY from inside the `tools` folder!** Otherwise, a lot of stuff will break.

The following sections explain the usage if these scripts.

### rename-project

As the title suggest, with this tool you could rename the whole project. It renames all of the files, folders and text inside files (so usings, namespaces, class names, variable names, etc.) to another given name.

Example: renaming the default (`ExamTemplate`) to `CarShop`
```bash
./rename-project.sh CarShop
```

If you've already renamed your project and want to change the name yet again, just give the script two parameters: the first one is the current name and the second - the new name.

Example: renaming the project called `CarShop` to `BookShop`
```bash
./rename-project.sh CarShop BookShop
```

You could rename stuff manually, but this and all other scripts rely on the naming structure that this script provides.

### add-feature-template

This is a scaffolding-like script. With it, you type in some information about the project and your desired feature and it generates most files you'll need in all layers (Data, Service, Web).

The script is meant to give you the initial stuff to get you started. Outside of creating some files and their **preset contents**, the script can't do anything else. Another limitation is references: it can't detect and add `using` statements, so if you use some fancy data type, make sure to add the appropriate `using`!

 **Example**: Let's say you [renamed](#rename-project) your project to `CarShop` and need a `Car` class that has a `Brand`, `Model` and `Year`.
- The first argument is the project name, then the name of your desired feature (should be the same name you would use when creating a database model) and then the different properties in the format `"type Name"`.
```bash
./add-feature-template.sh CarShop Car "string Brand" "string Model" "int Year"
```
The script will create (and edit where specified) the following files:
```
├─ Data
│  ├─ Models
│  │  └─ Car.cs
│  └─ CarShopContext.cs (Only edited; Adds a DbSet<Car>)
├─ Services
│  ├─ Configurations
│  │  └─ ServiceCarMappings.cs
│  ├─ Models
│  │  └─ Car
│  │     └─ CarServiceModel.cs
│  └─ CarService.cs
├─ Web
│  ├─ Configurations
│  │  └─ ControllerCarMappings.cs
│  ├─ Controllers
│  │  └─ CarControllers.cs
│  ├─ Models
│  │  └─ Car
│  │     ├─ CarViewModel.cs
│  │     ├─ CreateCarViewModel.cs
│  │     └─ EditCarViewModel.cs
│  └─ Views
│     ├─ Car
│     │  ├─ Index.cshtml
│     │  ├─ Create.cshtml
│     │  ├─ Edit.cshtml
│     │  └─ Delete.cshtml
│     └─ _ViewImports.cshtml (Only edited; Adds reference to the Car View Models namespace)
└─ Startup.cs (Only edited; Adds dependency injection configuration for CarService)
```
All of the models have all given properties **and a "Guid Id" property**. Only the Web Create models don't have the "Guid Id" property.

Controller and Service have **very basic** CRUD of the model. No view links are added, so you'll need to provide them (and to test out if everything work: just edit the link in the browser).

### delete-feature-template

As the name suggests, it just removes any feature, generated with [add-feature-template](#add-feature-template). Or more precisely, it removes all created files and removes references in edited files.

**Example**: Let's say we have a project named `CarShop` and we generated a feature called `Car` that we don't want anymore
- The first argument is the name of the project, and the second is that of the feature
```bash
./delete-feature-template.sh CarShop Car
```

### rename-namespaces

`rename-project` and `add-feature-template` use and require all namespaces to start with `ProjectName.` (so, for example, `CarShop.Data.Models`). If you want that first word to be changed to something else or entirely removed, you can use this script.

**Example**: rename all namespaces from `ExamTemplate....` to `MyShop....`
```bash
./rename-namespaces.sh MyShop
```

**Example**: rename all namespaces from `MyShop....` to `CarShop....`
```bash
./rename-namespaces.sh MyShop CarShop
```

**Example**: remove the start portion of all namespaces in the `CarShop` project (so form `CarShop.Data.Models` we have `Data.Models`)
```bash
./rename-namespaces.sh -r CarShop
```

## Notes

There are some configs that I've made that you may want to change. This section explains them.

### Database

The project is made to use [PostgreSQL](https://www.postgresql.org/) by default. Since not everyone would want to use that, here are some steps for other databases. **Side note**: delete the migrations if you want to use another database and add new ones!

Using MySQL:
1. Install the [MySql.Data.EntityFrameworkCore](https://www.nuget.org/packages/MySql.Data.EntityFrameworkCore/) NuGet package in **both** `ExamTemplate/Web` and `ExamTemplate/Data`
2. In the `ConfigureServices` method, inside `ExamTemplate/Web/Startup.cs`, **and** in the `CreateDbContext` method, inside `ExamTemplate/Data/TemplateContextFactory.cs` change `UseNpgsql` to `UseMySQL`
   - You'll also need to add a using for the package in step 1

Using SQL Server (MSSQL):
1. Install the [Microsoft.EntityFrameworkCore.SqlServer](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.SqlServer/) NuGet package in **both** `ExamTemplate/Web` and `ExamTemplate/Data`
2. In the `ConfigureServices` method, inside `ExamTemplate/Web/Startup.cs`, **and** in the `CreateDbContext` method, inside `ExamTemplate/Data/TemplateContextFactory.cs` change `UseNpgsql` to `UseSqlServer`
   - You'll also need to add a using for the package in step 1

### Cloud

The template uses [Cloudinary](https://cloudinary.com/) for source control. The logic for uploading a file is inside `ExamTemplate/Services/CloudinaryService.cs`.

If you need cloud somewhere, just dependency-inject the service and update your `ExamTemplate/Web/appsettings.Development.json` with the values from your own Cloudinary account.

Usage is simple: just call the `UploadFilesToCloud` method, and give it a `List<IFormFile>` (if you only need to upload one file, create a List and inside it put that file). It will return a List with the URLs of all given files.

This does mean that your view models (that require uploading a file) will need an `IFormFile` property. In the HTML, your file upload form should have a `<input type="file">`.

### HTTPS

HTTPS is disabled by default, since certificates could cause issues on some systems.

To revert:
1. Add `app.UseHttpsRedirection();` in the `Configure` method, inside `ExamTemplate/Web/Startup.cs`
2. Add `https://localhost:5001;` in `"applicationUrl"` in the `Web` section, inside `ExamTemplate/Web/Properties/launchSettings.json`.
   - So, line 21 in the `launchSettings.json` file should look like: `"applicationUrl": "https://localhost:5001;http://localhost:5000",`
