using System;

namespace ExamTemplate.Common
{
	public static class ErrorMessages
	{
		public static string NullObject(Type t)
		{
			return String.Format("{0} cannot be null!", t);
		}
	}
}
