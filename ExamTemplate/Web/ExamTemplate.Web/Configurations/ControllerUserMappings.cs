using AutoMapper;
using ExamTemplate.Services.Models.User;
using ExamTemplate.Web.Models.User;

namespace ExamTemplate.Services.Configurations
{
	public class ControllerUserMappings : Profile
	{
		public ControllerUserMappings()
		{
			CreateMap<RegisterUserWebModel, RegisterUserServiceModel>();
			CreateMap<LoginUserWebModel, LoginUserServiceModel>();
			CreateMap<UserServiceModel, UserWebModel>();
			CreateMap<UserServiceModel, EditUserWebModel>();
			CreateMap<EditUserWebModel, UserServiceModel>();
		}
	}
}
