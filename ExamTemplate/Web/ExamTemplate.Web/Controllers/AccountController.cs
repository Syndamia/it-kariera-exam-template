using ExamTemplate.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using ExamTemplate.Web.Models.User;
using AutoMapper;
using ExamTemplate.Services.Models.User;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace ExamTemplate.Web.Controllers
{
	[Authorize]
	public class AccountController : Controller
	{
		private readonly IMapper _autoMapper;
		private readonly IUserService _userService;

		public AccountController(IMapper autoMapper, IUserService userService)
		{
			this._autoMapper = autoMapper;
			this._userService = userService;
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult Register()
		{
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IActionResult> Register(RegisterUserWebModel registerUserWebModel)
		{
			if (!ModelState.IsValid)
				return View(registerUserWebModel);

			RegisterUserServiceModel registerUserServiceModel = this._autoMapper.Map<RegisterUserServiceModel>(registerUserWebModel);

			bool result = await this._userService.RegisterUserAsync(registerUserServiceModel);

			if (result)
				return await this.Login(new LoginUserWebModel { 
						Username = registerUserServiceModel.Username,
						Password = registerUserServiceModel.Password
						});
			else
				return View(registerUserWebModel);
		}

		[HttpGet]
		[AllowAnonymous]
		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		public async Task<IActionResult> Login(LoginUserWebModel loginUserWebModel)
		{
			if (!ModelState.IsValid)
				return View(loginUserWebModel);

			LoginUserServiceModel loginUserServiceModel = this._autoMapper.Map<LoginUserServiceModel>(loginUserWebModel);

			bool result = await this._userService.LoginUserAsync(loginUserServiceModel);

			if (result)
				return RedirectToAction("Index", "Home");
			else
				return View(loginUserWebModel);
		}

		[HttpPost]
		public async Task<IActionResult> Logout()
		{
			await this._userService.LogoutAsync();

			return RedirectToAction("Login");
		}

		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> Profile(string username)
		{
			UserServiceModel userServiceModel = await this._userService.GetUserByUsernameAsync(username);

			if (userServiceModel == default(UserServiceModel))
				return RedirectToAction("ErrorNotFound", "Home");

			UserWebModel userWebModel = this._autoMapper.Map<UserWebModel>(userServiceModel);

			return View(userWebModel);
		}

		[HttpGet]
		public async Task<IActionResult> Edit()
		{
			UserServiceModel userServiceModel = await this._userService.GetUserByClaimsAsync(this.HttpContext.User);

			if (userServiceModel == default(UserServiceModel))
				return RedirectToAction("ErrorNotFound", "Home");

			EditUserWebModel editUserWebModel = this._autoMapper.Map<EditUserWebModel>(userServiceModel);

			return View(editUserWebModel);
		}

		[HttpPost]
		public async Task<IActionResult> Edit(EditUserWebModel editUserWebModel)
		{
			if (!ModelState.IsValid)
				return View(editUserWebModel);

			if (!this._userService.IsSignedIn(HttpContext.User))
				return RedirectToAction("Login");

			UserServiceModel loggedInUser = await this._userService.GetUserByClaimsAsync(HttpContext.User);

			UserServiceModel userServiceModel = this._autoMapper.Map<UserServiceModel>(editUserWebModel);
			bool result = await this._userService.EditUserAsync(HttpContext.User, userServiceModel);

			if (result)
			{
				if (loggedInUser.Username != editUserWebModel.Username)
					await this._userService.LogoutAsync();

				return RedirectToAction("Profile", new { username = editUserWebModel.Username });
			}
			else
				return RedirectToAction("Profile", new { username = loggedInUser.Username });
		}

		[HttpPost]
		public async Task<IActionResult> Delete()
		{
			await this._userService.LogoutAsync();
			bool result = await this._userService.DeleteUserAsync(HttpContext.User);

			if (result)
				return RedirectToAction("Login");
			else
				return RedirectToAction("Index", "Home");
		}
	}
}
