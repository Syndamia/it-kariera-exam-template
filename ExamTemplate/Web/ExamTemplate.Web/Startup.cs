using System;
using System.Linq;
using ExamTemplate.Common;
using ExamTemplate.Data;
using ExamTemplate.Data.Models;
using ExamTemplate.Services.Services;
using ExamTemplate.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ExamTemplate.Web
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllersWithViews();
			services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

			/*
			 * Dependency Injection configuration
			 */

			services.AddTransient<ICloudinaryService, CloudinaryService>(options =>
				new CloudinaryService(
					cloudName: this.Configuration.GetSection("Cloud").GetSection("cloudName").Value,
					apiKey: this.Configuration.GetSection("Cloud").GetSection("apiKey").Value,
					apiSecret: this.Configuration.GetSection("Cloud").GetSection("apiSecret").Value));
			services.AddTransient<IUserService, UserService>();

			/* 
			 * Database configuration
			 */

			services.AddDbContext<TemplateContext>(options =>
				options.UseNpgsql(this.Configuration.GetConnectionString("LocalDBConnection")));

			// Needed for SignInManager and UserManager
			services.AddIdentity<User, IdentityRole<Guid>>(options =>
			{
				options.SignIn.RequireConfirmedAccount = false;

				// Password settings
				options.Password.RequireDigit = false;
				options.Password.RequireLowercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;
				options.Password.RequiredLength = 3;
				options.Password.RequiredUniqueChars = 0;
			}).AddRoles<IdentityRole<Guid>>()
				.AddEntityFrameworkStores<TemplateContext>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseStaticFiles();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
				endpoints.MapFallbackToController("ErrorNotFound", "Home");
			});

			/* 
			 * Make sure that the database is migrated
			 * and that the User and Admin role exist in database 
			 */

			using var serviceScope = app.ApplicationServices.CreateScope();
			using var dbContext = serviceScope.ServiceProvider.GetRequiredService<TemplateContext>();

			dbContext.Database.Migrate();

			var roleManager = (RoleManager<IdentityRole<Guid>>)serviceScope.ServiceProvider.GetService(typeof(RoleManager<IdentityRole<Guid>>));
			foreach (string name in RoleConst.GetAllNames())
			{
				if (!dbContext.Roles.Any(x => x.Name == name))
				{
					IdentityRole<Guid> role = new IdentityRole<Guid>() { Name = name };
					roleManager.CreateAsync(role).Wait();
				}
			}

			/* If you want to create some custom database values at startup
			 * uncomment the following code
			 * and replace OBJCONST_ with your static class with constants (e.g. RoleConst)
			 *     replace OBJS_ with the name of the DbSet of your database model (e.g. Roles)
			 *     replace OBJ_ with the name of your database model (e.g. Role)

			foreach (string name in OBJCONST_.GetAllNames())
			{
				if (!dbContext.OBJS_.Any(x => x.Name == name))
				{
					var entity = new OBJ_() { Id = Guid.NewGuid(), Name = name };
					dbContext.OBJS_.Add(entity);
					dbContext.SaveChanges();
				}
			}
			*/
		}
	}
}
