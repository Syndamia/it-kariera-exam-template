using System.ComponentModel.DataAnnotations;

namespace ExamTemplate.Web.Models.User
{
	public class LoginUserWebModel
	{
		[Required]
		[MinLength(1)]
		public string Username { get; set; }

		[Required]
		[MinLength(1)]
		public string Password { get; set; }
	}
}
