namespace ExamTemplate.Web.Models.User
{
	public class UserWebModel
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Username { get; set; }
	}
}
