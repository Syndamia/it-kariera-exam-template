using System.ComponentModel.DataAnnotations;

namespace ExamTemplate.Web.Models.User
{
	public class EditUserWebModel
	{
		[Required]
		[MinLength(1)]
		public string FirstName { get; set; }

		[Required]
		[MinLength(1)]
		public string LastName { get; set; }

		[Required]
		[MinLength(1)]
		public string Username { get; set; }
	}
}
