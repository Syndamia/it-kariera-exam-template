using System;

namespace ExamTemplate.Web.Models
{
	public abstract class BaseWebModel
	{
		public Guid Id { get; set; }
	}
}
