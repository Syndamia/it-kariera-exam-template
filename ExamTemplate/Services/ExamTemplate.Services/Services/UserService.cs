﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using ExamTemplate.Data;
using ExamTemplate.Data.Models;
using ExamTemplate.Services.Models.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using ExamTemplate.Services.Interfaces;
using ExamTemplate.Common;

namespace ExamTemplate.Services.Services
{
	public class UserService : IUserService
	{
		private readonly IMapper _autoMapper;
		private readonly TemplateContext _context;
		private readonly SignInManager<User> _signInManager;
		private readonly UserManager<User> _userManager;
		private readonly RoleManager<IdentityRole<Guid>> _roleManager;

		public UserService(IMapper autoMapper, TemplateContext templateContext, SignInManager<User> signInManager, UserManager<User> userManager, RoleManager<IdentityRole<Guid>> roleManager)
		{
			this._autoMapper = autoMapper;
			this._context = templateContext;
			this._signInManager = signInManager;
			this._userManager = userManager;
			this._roleManager = roleManager;
		}

		public async Task<bool> RegisterUserAsync(RegisterUserServiceModel registerUserServiceModel)
		{
			if (registerUserServiceModel == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(RegisterUserServiceModel)));

			User user = this._autoMapper.Map<User>(registerUserServiceModel);

			user.PasswordHash = this._userManager.PasswordHasher.HashPassword(user, registerUserServiceModel.Password);
			IdentityResult userCreateResult = await this._userManager.CreateAsync(user);

			if (!userCreateResult.Succeeded)
			{
				await this._userManager.DeleteAsync(user);
				return false;
			}

			IdentityResult addRoleResult;
			if (await this._userManager.Users.CountAsync() == 1) // 1, because we added the user in line 37
				addRoleResult = await this._userManager.AddToRoleAsync(user, RoleConst.Admin);
			else
				addRoleResult = await this._userManager.AddToRoleAsync(user, RoleConst.User);

			return addRoleResult.Succeeded;
		}

		public async Task<bool> LoginUserAsync(LoginUserServiceModel loginUserServiceModel)
		{
			if (loginUserServiceModel == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(LoginUserServiceModel)));

			SignInResult result = await this._signInManager.PasswordSignInAsync(loginUserServiceModel.Username, loginUserServiceModel.Password, false, false);

			return result.Succeeded;
		}

		public async Task LogoutAsync()
		{
			await this._signInManager.SignOutAsync();
		}

		public async Task<UserServiceModel> GetUserByUsernameAsync(string username)
		{
			if (username == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(string)));

			User user = await this._userManager.Users
				.FirstOrDefaultAsync(x => x.UserName == username);

			return this._autoMapper.Map<UserServiceModel>(user);
		}

		public async Task<UserServiceModel> GetUserByClaimsAsync(ClaimsPrincipal claimsPrincipal)
		{
			if (claimsPrincipal == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(ClaimsPrincipal)));

			User user = await this._userManager.GetUserAsync(claimsPrincipal);

			return this._autoMapper.Map<UserServiceModel>(user);
		}

		public async Task<bool> EditUserAsync(ClaimsPrincipal claimsPrincipal, UserServiceModel userServiceModel)
		{
			if (claimsPrincipal == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(ClaimsPrincipal)));
			if (userServiceModel == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(UserServiceModel)));

			User user = await this._userManager.GetUserAsync(claimsPrincipal);

			user.UserName = userServiceModel.Username;
			user.FirstName = userServiceModel.FirstName;
			user.LastName = userServiceModel.LastName;

			IdentityResult result = await this._userManager.UpdateAsync(user);
			return result.Succeeded;
		}

		public async Task<bool> DeleteUserAsync(ClaimsPrincipal claimsPrincipal)
		{
			if (claimsPrincipal == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(ClaimsPrincipal)));

			User user = await this._userManager.GetUserAsync(claimsPrincipal);

			IdentityResult result = await this._userManager.DeleteAsync(user);
			return result.Succeeded;
		}

		public bool IsSignedIn(ClaimsPrincipal claimsPrincipal)
		{
			if (claimsPrincipal == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(ClaimsPrincipal)));

			return this._signInManager.IsSignedIn(claimsPrincipal);
		}
	}
}
