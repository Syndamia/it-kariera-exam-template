using System;
using System.Threading.Tasks;
using AutoMapper;
using ExamTemplate.Data;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ExamTemplate.Services.Interfaces;
using ExamTemplate.Common;

namespace ExamTemplate.Services.Services
{
    public abstract class BaseService<DbModel, ServiceModel> : IBaseService<DbModel, ServiceModel>
		where DbModel : class
		where ServiceModel : class
	{
		protected IMapper _autoMapper { get; private set; }
		protected TemplateContext _context { get; private set; }

		protected BaseService(IMapper autoMapper, TemplateContext context)
		{
			this._autoMapper = autoMapper;
			this._context = context;
		}

		public virtual async Task<bool> CreateAsync(ServiceModel serviceModel)
		{
			if (serviceModel == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(ServiceModel)));

			DbModel newEntity = this._autoMapper.Map<DbModel>(serviceModel);

			await this.GetDbSet()
				.AddAsync(newEntity);

			return await this.SaveChangesAsync();
		}

		public virtual async Task<ServiceModel> GetByIdAsync(Guid id)
		{
			if (id == Guid.Empty)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(Guid)));

			DbModel entity = await this.GetDbSet()
				.FindAsync(id);

			return this._autoMapper.Map<ServiceModel>(entity);
		}

		public virtual async Task<List<ServiceModel>> GetAllAsync()
		{
			return await this.GetDbSet()
				.Select(x => this._autoMapper.Map<ServiceModel>(x))
				.ToListAsync();
		}

		public virtual async Task<bool> EditAsync(ServiceModel serviceModel)
		{
			if (serviceModel == null)
				throw new ArgumentNullException(ErrorMessages.NullObject(typeof(ServiceModel)));

			DbModel entity = this._autoMapper.Map<DbModel>(serviceModel);

			this._context.Update(entity);

			return await this.SaveChangesAsync();
		}

		public virtual async Task<bool> DeleteAsync(Guid id)
		{
			DbModel entity = this.GetDbSet()
				.Find(id);

			this._context.Remove(entity);

			return await this.SaveChangesAsync();
		}

		/// Get, Create, Edit and Delete use this method
		/// Override it to add include statements (and so all other methods will work with includes too)
		protected virtual DbSet<DbModel> GetDbSet()
		{
			return this._context.Set<DbModel>();
		}

		protected async Task<bool> SaveChangesAsync()
		{
			return await this._context.SaveChangesAsync() >= 1;
		}
	}
}
