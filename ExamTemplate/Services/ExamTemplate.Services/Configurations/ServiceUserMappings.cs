using AutoMapper;
using ExamTemplate.Data.Models;
using ExamTemplate.Services.Models.User;

namespace ExamTemplate.Services.Configurations
{
	public class ServiceUserMappings : Profile
	{
		public ServiceUserMappings()
		{
			CreateMap<RegisterUserServiceModel, User>();
			CreateMap<User, UserServiceModel>();
		}
	}
}
