using System.Security.Claims;
using System.Threading.Tasks;
using ExamTemplate.Services.Models.User;

namespace ExamTemplate.Services.Interfaces
{
	public interface IUserService
	{
		Task<bool> RegisterUserAsync(RegisterUserServiceModel registerUserServiceModel);

		Task<bool> LoginUserAsync(LoginUserServiceModel loginUserServiceModel);

		Task LogoutAsync();

		Task<UserServiceModel> GetUserByUsernameAsync(string username);

		Task<UserServiceModel> GetUserByClaimsAsync(ClaimsPrincipal claimsPrincipal);

		Task<bool> EditUserAsync(ClaimsPrincipal claimsPrincipal, UserServiceModel userServiceModel);

		Task<bool> DeleteUserAsync(ClaimsPrincipal claimsPrincipal);

		bool IsSignedIn(ClaimsPrincipal claimsPrincipal);
	}
}
