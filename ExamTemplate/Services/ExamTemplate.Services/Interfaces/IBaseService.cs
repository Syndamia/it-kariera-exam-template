using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ExamTemplate.Services.Interfaces
{
    public interface IBaseService<DbModel, ServiceModel>
		where DbModel : class
		where ServiceModel : class
	{
		Task<bool> CreateAsync(ServiceModel serviceModel);

		Task<ServiceModel> GetByIdAsync(Guid id);

		Task<List<ServiceModel>> GetAllAsync();

		Task<bool> EditAsync(ServiceModel serviceModel);

		Task<bool> DeleteAsync(Guid id);
	}
}
