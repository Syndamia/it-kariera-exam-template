using System;

namespace ExamTemplate.Services.Models
{
	public abstract class BaseServiceModel
	{
		public Guid Id { get; set; }
	}
}
