namespace ExamTemplate.Services.Models.User
{
	public class UserServiceModel
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Username { get; set; }
	}
}
