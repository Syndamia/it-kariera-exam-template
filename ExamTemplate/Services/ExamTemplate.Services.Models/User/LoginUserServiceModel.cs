namespace ExamTemplate.Services.Models.User
{
	public class LoginUserServiceModel
	{
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
