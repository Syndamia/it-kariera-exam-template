namespace ExamTemplate.Services.Models.User
{
	public class RegisterUserServiceModel
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
