﻿using System;
using ExamTemplate.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ExamTemplate.Data
{
	public class TemplateContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
	{
		public TemplateContext(DbContextOptions<TemplateContext> options)
			: base(options) { }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<User>()
				.HasIndex(x => x.UserName)
				.IsUnique();

			base.OnModelCreating(builder);
		}
	}
}
