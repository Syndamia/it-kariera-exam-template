using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace ExamTemplate.Data
{
	public class TemplateContextFactory : IDesignTimeDbContextFactory<TemplateContext>
	{
		public TemplateContext CreateDbContext(string[] args)
		{
			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory() + "/../../Web/CarShop.Web/")
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddJsonFile("appsettings.Development.json", optional: true)
				.Build();

			var optionsBuilder = new DbContextOptionsBuilder<TemplateContext>()
				.UseNpgsql(configuration.GetConnectionString("LocalDBConnection"));

			return new TemplateContext(optionsBuilder.Options);
		}
	}
}
